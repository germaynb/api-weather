# Information about the application

## Commands

- php artisan current Havana,CU -uimperial


Get the current weather data for the given location.
Arguments:
•	Location, in {city},{country code} format. If you don't pass the argument, it defaults to Santander,ES
Options:
•	--units: Units of measurement must be metric or imperial, metric by default.


- php artisan forecast Madrid,ES -d5 -uimperial

Get the weather forecast for max 5 days for the given location.
Arguments:
•	Location, in {city},{country code} format. If you don't pass the argument, it defaults to Santander,ES
Options:
•	--days: The number of days to retrieve forecast data for, 1 by default.
•	--units: Units of measurement must be metric or imperial, metric by default.


- php artisan forecast:ask

Same as forecast command using questions for days and unit of measure



## Endponit
**POST** CurrentWeather

- Url = {{url}}/api/current/weather

- Body (json)

{
    "city":"Santander",
    "country_code" : "ES",
    "unit":"imperial"
}

**POST** ForecastWeather

- Url = {{url}}/api/forecast/weather

- Body (json

{
    "city":"Santander",
    "country_code" : "ES",
    "unit":"metric",
    "days":"5"
}




