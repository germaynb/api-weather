<?php

namespace App\Console\Commands;

use Dnsimmons\OpenWeather\OpenWeather;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Weather\openweatherApi;

class CurrentWeather extends Command
{
      /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'current 
            {city=Santander : Name of the city to consult}
            {country_code=ES : Code of the city to consult}
            {--u|unit=imperial}'
            ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Application that implements console commands that access the openweathermap api to obtain weather data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $weather = new OpenWeather();
        $current = $weather->getCurrentWeatherByCityName($this->argument('city'),$this->option('unit'));

        $print = new openweatherApi();
        $print->printWeather( $current['location']['name'],$current['location']['country'],date('F j, Y', $current['datetime']['timestamp']),$current['condition']['desc'],$current['forecast']['temp'],$this->option('unit'));

    }
}
