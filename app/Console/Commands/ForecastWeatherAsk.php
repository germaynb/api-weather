<?php

namespace App\Console\Commands;

use Dnsimmons\OpenWeather\OpenWeather;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Weather\openweatherApi;

class ForecastWeatherAsk extends Command
{
  /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'forecast:ask
            {city=Santander : Name of the city to consult}
            {country_code=ES : Code of the city to consult}
            {--d|day=1}
            {--u|unit=imperial}
            '
            ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Application that implements console commands that access the openweathermap api to obtain weather data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $days = $this->ask('How many days to forecast?',1);

        
        if($days > 5){
            $this->error('You exceeded the limit of days for the forecast!');
            return 0;
        }

        $unit = $this->choice(
            'What unit of measure?',
            ['metric', 'imperial'],
            1
        );

        $weather = new OpenWeather();
        $forecast = $weather->getForecastWeatherByCityName($this->argument('city'),$unit);


        for ($i = 0; $i < $days; $i++) {
            $current = $forecast['forecast'][$i];
            $print = new openweatherApi();
            $print->printWeather($forecast['location']['name'],$forecast['location']['country'],date('F j, Y', $current['datetime']['timestamp']),$current['condition']['desc'],$current['forecast']['temp'],$unit);
        }



    }

}
