<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Dnsimmons\OpenWeather\OpenWeather;
use App\Weather\openweatherApi;

class ApiController extends Controller
{
    public function currentWeather(Request $request)
    {

        $weather = new OpenWeather();
        $current = $weather->getCurrentWeatherByCityName($request->city,$request->unit);

        $print = new openweatherApi();
        return $print->printWeatherApi( $current['location']['name'],$current['location']['country'],date('F j, Y', $current['datetime']['timestamp']),$current['condition']['desc'],$current['forecast']['temp'],$request->unit);
    }

    public function forecastWeather(Request $request)
    {
        $days = $request->days;

        $weather = new OpenWeather();
        $forecast = $weather->getForecastWeatherByCityName($request->city,$request->unit);

        if($days > 5){
            response()->json(['errors' => ['email' => ['The email is invalid.']]], 422);
        }

        $forecast_respose = [];

        for ($i = 0; $i < $days; $i++) {
            $current = $forecast['forecast'][$i];
            $print = new openweatherApi();
            $forecast_respose [] = $print->printWeatherApi($forecast['location']['name'],$forecast['location']['country'],date('F j, Y', $current['datetime']['timestamp']),$current['condition']['desc'],$current['forecast']['temp'],$request->unit);
        }
        return $forecast_respose ;

    }


}
