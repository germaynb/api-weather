<?php 
namespace App\Weather;

class openweatherApi
{
    public function printWeather($city,$country_code,$date,$weather_desc,$temp,$unit)
    {
        $unit_desc = $unit == 'imperial' ? '°F' : ($unit == 'metric' ? '°C' : ($unit == 'kevin' ? '°K' :'°F'));
        echo "
        $city ($country_code) 
        $date 
        > Weather: $weather_desc
        > Temperature: $temp $unit_desc";
    }


    public function printWeatherApi($city,$country_code,$date,$weather_desc,$temp,$unit)
    {
        $unit_desc = $unit == 'imperial' ? '°F' : ($unit == 'metric' ? '°C' : ($unit == 'kevin' ? '°K' :'°F'));

        return [
            'city' => $city ,
            'country' => $country_code,
            'date' => $date,
            'weather' => $weather_desc,
            'temperature' => "$temp $unit_desc",
        ];

    }

}